﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Match_3
{
    public class SpriteData : Game
    {
        public Point Dimension { get { return dimension; } }
        public Point PartSize { get { return partSize; } }
        private Texture2D texture;
        private Point size;
        private Point partSize;     //Размеры одного кадра
        private Point dimension;    //Количество кадров в длину и ширину
        private SpriteBatch spriteBatch;
        private Rectangle spriteRect;


        public SpriteData(Texture2D _texture, SpriteBatch _spriteBatch)
        {
            texture = _texture;
            size = new Point(texture.Width, texture.Height);
            partSize = size;
            dimension = new Point(1, 1);
            spriteBatch = _spriteBatch;
            spriteRect = new Rectangle(0, 0, partSize.X, partSize.Y);
        }

        public SpriteData(Texture2D _texture, SpriteBatch _spriteBatch, Point spriteDimension)
        {
            texture = _texture;
            dimension = spriteDimension;
            size = new Point(texture.Width, texture.Height);
            partSize = new Point(size.X / dimension.X, size.Y / dimension.Y);
            spriteBatch = _spriteBatch;
            spriteRect = new Rectangle(0, 0, partSize.X, partSize.Y);
        }

        public void Draw(Point point, Point frame) //Отрисовка части спрайта в указанной точке
        {
            Rectangle sourceFrame = new Rectangle(partSize.X * (frame.X - 1), partSize.Y * (frame.Y - 1), partSize.X, partSize.Y);

            spriteBatch.Draw(texture, point.ToVector2(), sourceFrame, Color.White);
        }

        public void Draw(Point point) //Отрисовка всего спрайта в указанной точке
        {
            spriteBatch.Draw(texture, point.ToVector2(), Color.White);
        }
        public void Draw(Rectangle rect, Point frame) //отрисовка части спрайта в указанном прямоугольнике с масштабированием
        {
            Rectangle sourceFrame = new Rectangle(partSize.X * (frame.X - 1), partSize.Y * (frame.Y - 1), partSize.X, partSize.Y);

            spriteBatch.Draw(texture, rect, sourceFrame, Color.White);
        }

        public void Draw(Rectangle rect) //Отрисовка всего спрайта в указанном прямоугольнике с масштабированием
        {
            spriteBatch.Draw(texture, rect, Color.White);
        }
    }
}
