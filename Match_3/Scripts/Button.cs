﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Match_3
{
    public class Button
    {
        public SpriteData Current;  
        public Point Position;
        private SpriteData Normal;
        private SpriteData Hovered;
        private bool isHovered;

        public Button(SpriteData normal, SpriteData hovered, Point position)
        {
            Normal = normal;
            Hovered = hovered;
            Position = position;
            isHovered = false;
            Current = Normal;
        }

        public bool InBound(MouseState state)  //Проверяем, попадает ли курсор в область кнопки. Если да, то отображается вторая текстура
        {
            if (state.X < Position.X + Normal.PartSize.X &&
                     state.X > Position.X &&
                     state.Y < Position.Y + Normal.PartSize.Y &&
                     state.Y > Position.Y)
            {
                Current = Hovered;
                isHovered = true;
                return true;
            }
            Current = Normal;
            isHovered = false;
            return false;
        }

        public bool Clicked(MouseState currentMouseState)  //Нажатие срабатывает, если кнопка подсвечена
        {
            if (isHovered)
                return true;
            return false;
        }
    }
}
