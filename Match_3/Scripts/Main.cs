﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Match_3
{
    public enum GameState //Фазы игры
    {
        Menu,
        Gameplay,
        EndOfGame,
    }

    public enum FigureType //Типы фигур
    {
        Triangle,
        Cross,
        Round,
        Square,
        Rhombus
    }

    public enum Stage //Состояния смены ячеек и поиска совпадений
    {
        Normal,
        Swap,
        Match,
        Fill,
        Falling
    }

    public class Main : Game
    {
        GameState State;    
        Point GridSize;
        Point Selected_1st;                 //Первый выделенный элемент
        Point Selected_2nd;                 //Второй выделенный элемент
        Point NotSelected;                  //Выделение отсутствует
        int FallTime = 300;                 //Скорость падения фигур
        int RoundTime = 60;                 //Длительность игрового раунда
        int TimeRemain;                     //Оставшееся время игрового раунда
        int CurrentTime;                    //Время падения фигур
        int Cellsize;                       //Визуальный размер ячейки
        int Score;                          //Игровой счёт
        int[,] LinesVert;                   //Массив совпадений в столбцах
        int[,] LinesHoriz;                  //Массив совпадений в строках
        GraphicsDeviceManager graphics;     //Сведения о графическом адаптере
        SpriteBatch spriteBatch;            //Настройки отрисовки спрайтов
        Rectangle GameScreen;               //Размеры дисплея
        Rectangle MatrixRect;               //Размеры сетки с фигурами
        MouseState lastMouseState;          //Предыдущее состояние мыши
        bool Initialized = false;           //Инициализация данных до начала игровой фазы
        bool Returning = false;             //Отмена обмена ячеек
        Random Rand;                        //Случайное число
        Stage Stage = Stage.Normal;         //Состояние обмена и поиска совпадений

        SpriteData Triangle;                //Наборы текстур
        SpriteData Round;
        SpriteData Cross;
        SpriteData Square;
        SpriteData Rhombus;

        SpriteData PlayNormal;
        SpriteData PlayHovered;
        SpriteData OkNormal;
        SpriteData OkHovered;
        SpriteData Header;
        SpriteData Footer;
        SpriteData BackGround;
        SpriteData GameOver;

        SpriteFont Font;                    //Шрифт

        Item[,] ItemGrid;                   //Массив фигур
        Button PlayBtn;                     //Кнопка Play
        Button OkBtn;                       //Кнопка OK

        private Item AddItem(Point position)    //Добавляем в массив новый элемент со случайной фигурой
        {
            SpriteData sprite = null;
            FigureType type = new FigureType();

            switch (Rand.Next(0, 5))
            {
                case (int)FigureType.Triangle:
                    sprite = Triangle;
                    type = FigureType.Triangle;
                    break;
                case (int)FigureType.Cross:
                    sprite = Cross;
                    type = FigureType.Cross;
                    break;
                case (int)FigureType.Round:
                    sprite = Round;
                    type = FigureType.Round;
                    break;
                case (int)FigureType.Square:
                    sprite = Square;
                    type = FigureType.Square;
                    break;
                case (int)FigureType.Rhombus:
                    sprite = Rhombus;
                    type = FigureType.Rhombus;
                    break;
            }
            return new Item(sprite, position, type);
        }

        private void SetGrid()      //Первичное заполнение массива фигур. Поиск и устранение совпадений.
        {
            bool fnd = true;
            while (fnd)
            {
                fnd = false;
                for (int i = 0; i < GridSize.X; i++)
                    for (int j = 0; j < GridSize.Y; j++)
                    {
                        if (ItemGrid[i, j] == null)
                            ItemGrid[i, j] = AddItem(new Point(i * Triangle.PartSize.X, j * Triangle.PartSize.Y));
                    }

                LinesHoriz = new int[GridSize.X, GridSize.Y];
                LinesVert = new int[GridSize.X, GridSize.Y];
                for (int i = 0; i < GridSize.X; i++)
                {
                    SrcHoriz(i, 0);
                    SrcVert(0, i);
                }

                for (int i = 0; i < GridSize.X; i++)
                    for (int j = 0; j < GridSize.Y; j++)
                    {
                        if (LinesHoriz[i, j] > 2)
                        {
                            fnd = true;
                            for (int h = j; h < j + LinesHoriz[i, j]; h++)
                            {
                                ItemGrid[i, h] = null;
                            }
                        }
                        if (LinesVert[j, i] > 2)
                        {
                            fnd = true;
                            for (int v = j; v < j + LinesVert[j, i]; v++)
                            {
                                ItemGrid[v, i] = null;
                            }
                        }
                    }
            }
        }

        private void Select(Point element)      //Выделение элемента на игровой сетке
        {
            if (Selected_1st == NotSelected)    //Нет выделенных элементов. Выделяем элемент.
            {
                Selected_1st = element;
                ItemGrid[element.X, element.Y].Select();
            }
            else
            {
                if (Selected_1st == element)       //Выбрали выделенный элемент. Снимаем выделение.
                {
                    ItemGrid[element.X, element.Y].Select();
                    Selected_1st = NotSelected;
                }
                else
                {
                    if ((Math.Abs(Selected_1st.X - element.X) == 1) ^ (Math.Abs(Selected_1st.Y - element.Y) == 1))  //Выделено два соседних элемента. Начинаем обмен
                    {
                        Selected_2nd = element;
                        Stage = Stage.Swap;
                        ItemGrid[Selected_1st.X, Selected_1st.Y].Select();
                        ItemGrid[Selected_1st.X, Selected_1st.Y].StartSwap();
                        ItemGrid[Selected_2nd.X, Selected_2nd.Y].StartSwap();
                    }
                    else          //Выделено два не соседних элемента. Снимаем выделение с первого.
                    {
                        ItemGrid[Selected_1st.X, Selected_1st.Y].Select();
                        ItemGrid[element.X, element.Y].Select();
                        Selected_1st = element;
                    }
                }
            }
        }

        private void Swap() //Меняем местами фигуры в массиве
        {
            Item tmp = ItemGrid[Selected_1st.X, Selected_1st.Y];
            ItemGrid[Selected_1st.X, Selected_1st.Y] = ItemGrid[Selected_2nd.X, Selected_2nd.Y];
            ItemGrid[Selected_2nd.X, Selected_2nd.Y] = tmp;
            ItemGrid[Selected_1st.X, Selected_1st.Y].ContinueSwap();
            ItemGrid[Selected_2nd.X, Selected_2nd.Y].ContinueSwap();
        }

        private bool Match()    //Поиск совпадений
        {
            LinesHoriz = new int[GridSize.X, GridSize.Y];
            LinesVert = new int[GridSize.X, GridSize.Y];
            for (int i = 0; i < GridSize.X; i++)
            {
                SrcHoriz(i, 0);
                SrcVert(0, i);
            }
            return ClearLines();
        }

        private bool ClearLines()   //Очищаем найденные совпадения и начисляем игровые очки
        {
            bool fnd = false;
            for (int i = 0; i < GridSize.X; i++)
                for (int j = 0; j < GridSize.Y; j++)
                {
                    if (LinesHoriz[i, j] > 2)
                    {
                        fnd = true;
                        Score += (LinesHoriz[i, j] - 1) * 50;
                        for (int h = j; h < j + LinesHoriz[i, j]; h++)
                        {
                                ItemGrid[i, h] = null;
                        }
                    }
                    if (LinesVert[j, i] > 2)
                    {
                        fnd = true;
                        Score += (LinesVert[j, i] - 1) * 50;
                        for (int v = j; v < j + LinesVert[j, i]; v++)
                        {
                                ItemGrid[v, i] = null;
                        }
                    }
                }
            return fnd;
        }

        private bool FillSpaces()       //Заполняем пустоты, появившиеся в ходе удаления совпадений
        {
            bool fnd = false;
            for (int j = 0; j < GridSize.Y; j++)
                for (int i = GridSize.X - 1; i >= 0; i--)
                    if (ItemGrid[i, j] == null)
                    {
                        fnd = true;
                        if (i > 0)
                            for (int k = i; k > 0; k--)
                            {
                                ItemGrid[k, j] = ItemGrid[k - 1, j];
                                if (ItemGrid[k, j] != null)
                                    ItemGrid[k, j].isFalling = true;
                            }
                        ItemGrid[0, j] = AddItem(new Point(0, j * Triangle.PartSize.Y));
                        ItemGrid[0, j].isFalling = true;
                        Stage = Stage.Falling;
                        CurrentTime = 0;
                        break;
                    }
            return fnd;
        }

        private void SrcHoriz(int row, int col)     //Поиск совпадений по горизонтали
        {
            for (int i = col; i < GridSize.Y; i++)
                if (ItemGrid[row, col].Type == ItemGrid[row, i].Type)
                {
                    LinesHoriz[row, col]++;
                }
                else
                {
                    SrcHoriz(row, i);
                    return;
                }
        }

        private void SrcVert(int row, int col)  //Поиск совпадений по вертикали
        {
            for (int i = row; i < GridSize.X; i++)
                if (ItemGrid[row, col].Type == ItemGrid[i, col].Type)
                {
                    LinesVert[row, col]++;
                }
                else
                {
                    SrcVert(i, col);
                    return;
                }
        }

        public Main()
        {
            graphics = new GraphicsDeviceManager(this);
            
            Content.RootDirectory = "Content";
            IsMouseVisible = true;
        }

        protected override void Initialize()
        {
            graphics.PreferredBackBufferHeight = graphics.GraphicsDevice.DisplayMode.Height;    //Устанавливаем полноэкранный режим
            graphics.PreferredBackBufferWidth = graphics.GraphicsDevice.DisplayMode.Width;      //с актуальным разрешением
            graphics.IsFullScreen = true;            
            graphics.ApplyChanges();

            State = GameState.Menu;
            GameScreen = new Rectangle(0, 0, Window.ClientBounds.Width, Window.ClientBounds.Height);

            GridSize = new Point(8, 8);
            ItemGrid = new Item[GridSize.X, GridSize.Y];
            NotSelected = new Point(-1, -1);
            Rand = new Random();

            base.Initialize();
        }

        protected override void LoadContent()   //Загрузка текстур
        {
            spriteBatch = new SpriteBatch(GraphicsDevice);

            Triangle = new SpriteData(this.Content.Load<Texture2D>("triangle"), spriteBatch, new Point(4, 2));
            Round = new SpriteData(this.Content.Load<Texture2D>("round"), spriteBatch, new Point(4, 2));
            Cross = new SpriteData(this.Content.Load<Texture2D>("cross"), spriteBatch, new Point(4, 2));
            Square = new SpriteData(this.Content.Load<Texture2D>("square"), spriteBatch, new Point(4, 2));
            Rhombus = new SpriteData(this.Content.Load<Texture2D>("rhombus"), spriteBatch, new Point(4, 2));
            PlayNormal = new SpriteData(this.Content.Load<Texture2D>("playbutton1"), spriteBatch);
            PlayHovered = new SpriteData(this.Content.Load<Texture2D>("playbutton2"), spriteBatch);
            Header = new SpriteData(this.Content.Load<Texture2D>("header"), spriteBatch);
            Footer = new SpriteData(this.Content.Load<Texture2D>("footer"), spriteBatch);
            BackGround = new SpriteData(this.Content.Load<Texture2D>("background_light"), spriteBatch);
            OkNormal = new SpriteData(this.Content.Load<Texture2D>("okbutton1"), spriteBatch);
            OkHovered = new SpriteData(this.Content.Load<Texture2D>("okbutton2"), spriteBatch);
            Font = this.Content.Load<SpriteFont>("Courier New");
            GameOver = new SpriteData(this.Content.Load<Texture2D>("gameover"), spriteBatch);

            PlayBtn = new Button(PlayNormal, PlayHovered, new Point(GameScreen.Width / 2 - PlayNormal.PartSize.X / 2, GameScreen.Height / 2 - PlayNormal.PartSize.Y / 2));
            OkBtn = new Button(OkNormal, OkHovered, new Point(GameScreen.Width / 2 - PlayNormal.PartSize.X / 2, GameScreen.Height / 2 - PlayNormal.PartSize.Y / 2));
        }

        protected override void UnloadContent()
        {
            
        }

        protected override void Update(GameTime gameTime)
        {
            if (IsActive)   //Если нет фокуса на игровом окне, то прекращаем обновлять игру
            {
                if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                    Exit();         //Выход по нажатию Esc

                switch (State)  //Разделение на три игровых цикла
                {
                    case GameState.Menu:
                        UpdateMenu(gameTime);
                        break;
                    case GameState.Gameplay:
                        UpdateGameplay(gameTime);
                        break;
                    case GameState.EndOfGame:
                        UpdateEndOfGame(gameTime);
                        break;
                }

                base.Update(gameTime);
            }
        }

        private void UpdateMenu(GameTime gameTime)      //Обновление логики меню
        {
            if (!Initialized)   //Первоначальная инициализация переменных
            {
                int tmp;
                if (graphics.GraphicsDevice.DisplayMode.AspectRatio > 1)
                {
                    tmp = GameScreen.Height - Header.PartSize.Y * 2;
                    tmp -= tmp % 8;
                    MatrixRect = new Rectangle((GameScreen.Center.X - tmp / 2), Header.PartSize.Y, tmp, tmp);
                }
                else
                {
                    tmp = GameScreen.Width % 8;
                    MatrixRect = new Rectangle(0, Header.PartSize.Y, tmp, tmp);
                }
                Cellsize = MatrixRect.Width / 8;

                Selected_1st = NotSelected;
                Selected_2nd = NotSelected;
                Score = 0;
                TimeRemain = RoundTime * 1000;      //Перевод в миллисекунды
                Initialized = true;
            }
            
            MouseState currentMouseState = Mouse.GetState(Window);  //Отслеживание положения курсора относительно кнопки и её нажатия
            if (currentMouseState != lastMouseState)
            {
                if ((currentMouseState.X != lastMouseState.X) || (currentMouseState.Y != lastMouseState.Y))
                    PlayBtn.InBound(currentMouseState);
                if ((currentMouseState.LeftButton != lastMouseState.LeftButton) && (currentMouseState.LeftButton == ButtonState.Pressed))
                    if (PlayBtn.Clicked(currentMouseState))
                    {
                        State = GameState.Gameplay;
                        SetGrid();
                    }
                lastMouseState = currentMouseState;
            }
        }

        private void UpdateGameplay(GameTime gameTime)      //Обновление игровой логики
        {
            Point element;
            MouseState currentMouseState = Mouse.GetState(Window);

            if (TimeRemain > 0)
                TimeRemain -= gameTime.ElapsedGameTime.Milliseconds;
            else
            {
                State = GameState.EndOfGame;
                return;
            }

            if ((currentMouseState != lastMouseState) && (Stage == Stage.Normal))    //Отслеживание положения курсора относительно фигур
            {
                if ((currentMouseState.LeftButton != lastMouseState.LeftButton) &&
                    (currentMouseState.LeftButton == ButtonState.Pressed))
                {
                    if (currentMouseState.X < MatrixRect.Right &&
                        currentMouseState.X > MatrixRect.X &&
                        currentMouseState.Y < MatrixRect.Bottom &&
                        currentMouseState.Y > MatrixRect.Y)
                    {
                        element = new Point((currentMouseState.Y - MatrixRect.Y) / Cellsize,
                            (currentMouseState.X - MatrixRect.X) / Cellsize);

                        Select(element);

                    }
                }
                lastMouseState = currentMouseState;
            }

            switch(Stage)       
            {
                case Stage.Swap:        //Обмен ячеек и проверка совпадений. Если не найдено, то возвращаем обратно.
                    if (!(ItemGrid[Selected_1st.X, Selected_1st.Y].isSwapping || ItemGrid[Selected_2nd.X, Selected_2nd.Y].isSwapping))
                        if (!Returning)
                        {
                            if (Match())
                            {
                                Stage = Stage.Fill;
                                Selected_1st = NotSelected;
                                Selected_2nd = NotSelected;
                                break;
                            }
                            else
                            {
                                Returning = true;
                                ItemGrid[Selected_1st.X, Selected_1st.Y].StartSwap();
                                ItemGrid[Selected_2nd.X, Selected_2nd.Y].StartSwap();

                            }
                        }
                        else
                        {
                            Stage = Stage.Normal;
                            Returning = false;
                            Selected_1st = NotSelected;
                            Selected_2nd = NotSelected;
                            break;
                        }
                    if (ItemGrid[Selected_1st.X, Selected_1st.Y].isSwapped && ItemGrid[Selected_2nd.X, Selected_2nd.Y].isSwapped)
                    {
                        Swap();
                    }
                    break;
                case Stage.Fill:        //Заполнение пустых ячеек
                    if (FillSpaces())
                        Stage = Stage.Falling;
                    else if (Match())
                        Stage = Stage.Fill;
                    else
                        Stage = Stage.Normal;

                    break;
                case Stage.Falling:     //Ожидание смещения фигур вниз
                    CurrentTime += gameTime.ElapsedGameTime.Milliseconds;
                    if (CurrentTime >= FallTime)
                    {
                        Stage = Stage.Fill;
                        for (int i = 0; i < GridSize.X; i++)
                            for (int j = 0; j < GridSize.Y; j++)
                                if (ItemGrid[i, j] != null)
                                    ItemGrid[i, j].isFalling = false;
                    }
                    break;
            }

            for (int i = 0; i < GridSize.X; i++)
                for (int j = 0; j < GridSize.Y; j++)
                {
                    if (ItemGrid[i, j] != null)
                        ItemGrid[i, j].UpdateAnim(gameTime);
                }
        }

        private void UpdateEndOfGame(GameTime gameTime)     //Обновление логики послеигрового окна
        {
            MouseState currentMouseState = Mouse.GetState(Window);
            if (currentMouseState != lastMouseState)
            {
                if ((currentMouseState.X != lastMouseState.X) || (currentMouseState.Y != lastMouseState.Y))
                    OkBtn.InBound(currentMouseState);
                if ((currentMouseState.LeftButton != lastMouseState.LeftButton) && (currentMouseState.LeftButton == ButtonState.Pressed))
                    if (OkBtn.Clicked(currentMouseState))
                    {
                        State = GameState.Menu;
                    }
                lastMouseState = currentMouseState;
            }
        }
    

        protected override void Draw(GameTime gameTime)     //Отрисовка игры
        {
            GraphicsDevice.Clear(Color.LightGray);
            spriteBatch.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend);

            switch (State)      //Разделение на три игровых цикла
            {
                case GameState.Menu:
                    DrawMenu(gameTime);
                    break;
                case GameState.Gameplay:
                    DrawGameplay(gameTime);
                    break;
                case GameState.EndOfGame:
                    DrawEndOfGame(gameTime);
                    break;
            }
            spriteBatch.End();

            base.Draw(gameTime);
        }

        private void DrawMenu(GameTime gameTime)        //Отрисовка кнопки Play
        {
            PlayBtn.Current.Draw(PlayBtn.Position);
        }

        private void DrawGameplay(GameTime gameTime)        //Отрисовка Фона, ячеек, фигур, панелей и надписей
        {
            BackGround.Draw(MatrixRect);
            for (int i = 0; i < GridSize.X; i++)
                for (int j = 0; j < GridSize.Y; j++)
                {
                    if (ItemGrid[i, j] != null)
                        if (ItemGrid[i, j].isFalling)
                        {
                            ItemGrid[i, j].Sprite.Draw(new Rectangle((MatrixRect.X + j * Cellsize),
                                (MatrixRect.Y + i * Cellsize - (int)Math.Ceiling(Cellsize * (1 - (float)CurrentTime / FallTime))), Cellsize, Cellsize),
                                ItemGrid[i, j].Frame);
                        }
                        else
                        {
                            ItemGrid[i, j].Sprite.Draw(new Rectangle((MatrixRect.X + j * Cellsize), (MatrixRect.Y + i * Cellsize), Cellsize, Cellsize),
                                ItemGrid[i, j].Frame);
                        }
                }

            Header.Draw(new Rectangle(MatrixRect.X, 0, MatrixRect.Width, Header.PartSize.Y));
            Footer.Draw(new Rectangle(MatrixRect.X, MatrixRect.Bottom, MatrixRect.Width, GameScreen.Height - MatrixRect.Bottom));

            spriteBatch.DrawString(Font, "Score: " + Score, new Vector2(MatrixRect.X + 10, 0), Color.Black);
            spriteBatch.DrawString(Font, "Remaining time: " + (TimeRemain / 1000), new Vector2(MatrixRect.Right - 245, 0), Color.Black);
        }

        private void DrawEndOfGame(GameTime gameTime)       //Отрисовка Кнопки и надписей послеигрового меню
        {
            OkBtn.Current.Draw(OkBtn.Position);
            GameOver.Draw(new Rectangle(MatrixRect.X, Cellsize, MatrixRect.Width, GameOver.PartSize.Y));
            spriteBatch.DrawString(Font, "Score: " + Score, new Vector2(OkBtn.Position.X + 40, OkBtn.Position.Y - 50), Color.White);
        }

    }
}
