﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Match_3
{
    public class Item
    {
        public SpriteData Sprite;
        public FigureType Type;
        public Point Frame;
        public Point Position;
        public bool isSwapped;
        public bool isFalling;
        public bool isSwapping;
        private bool isAnimate;
        private bool isSelected;
        private int currentTime; // сколько времени прошло
        private int period;      // период обновления в миллисекундах


        public Item(SpriteData sprite, Point position, FigureType type)
        {
            Sprite = sprite;
            Type = type;
            Frame = new Point(1, 1);
            isAnimate = false;
            isSelected = false;
            currentTime = 0;
            period = 50;
            Position = position;
            isSwapped = false;
            isSwapping = false;
            isFalling = false;
        }

        public void UpdateAnim(GameTime gameTime)  //Анимация спрайтлиста
        {
            if (!(isSwapping && isSwapped))
                if (isAnimate)
                {
                    // добавляем к текущему времени прошедшее время
                    currentTime += gameTime.ElapsedGameTime.Milliseconds;
                    // если текущее время превышает период обновления спрайта
                    if (currentTime > period)
                    {
                        currentTime -= period; // вычитаем из текущего времени период обновления
                        Frame.X++;
                        if (Frame.X > Sprite.Dimension.X) //Учитываем размерность спрайтлиста
                        {
                            Frame.X = 1;
                            Frame.Y++;
                            if ((Frame.X == 1) && (Frame.Y == 2)) // Нужный кадр для обмена ячеек
                            {
                                isSwapped = true;
                            }

                            if (Frame.Y > Sprite.Dimension.Y) //Учитываем размерность спрайтлиста
                            {
                                Frame.Y = 1;
                                isSwapping = false;
                                if (!isSelected)
                                {
                                    isAnimate = false;
                                    currentTime = 0;
                                }
                            }
                        }
                    }
                }
        }

        public void StartSwap()  //Вращение фигуры при обмене ячеек разделено на две фазы. До и после обмены позициями.
        {
            if (!isSwapping)
            {
                Frame = new Point(1, 1);
                isAnimate = true;
                isSelected = false;
                currentTime = 0;
                isSwapped = false;
                isSwapping = true;
            }
        }

        public void ContinueSwap()  //Вторая часть анимации
        {
            currentTime = 0;
            isSwapped = false;
        }

        public bool Select()  //Выделение фигуры для вращения
        {
            if (isSelected = !isSelected)
                isAnimate = true;
            return isSelected;
        }
    }
}
